package com.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.model.Product;

@Service
public class ProductDAO {
	
	//Dependency Injection for ProductRepository: can be implemented using @Autowried Annotation
	
	@Autowired
	ProductRepository prodRepo;
	
	public List<Product> getAllProducts() {
		return prodRepo.findAll();
	}

	public Product getProductById(int productId) {
		// TODO Auto-generated method stub
//		Product product = new Product(0,"Product not found...!!!", 0.0);
		return prodRepo.findById(productId).orElse(new Product());
	}
	
	public Product getProductByName(String prodName) {
	    return prodRepo.findByName(prodName);
	}

	public Product addProduct(Product product) {
	    return prodRepo.save(product);
	}
	public Product updateProduct(Product product) {
	    return prodRepo.save(product);
	}
	public void deleteProductById(int productid) {
	    prodRepo.deleteById(productid);
	}




}
