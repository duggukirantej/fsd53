package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.dao.EmployeeDAO;
import com.model.Employee;

@WebServlet("/EmployeeProfile")
public class EmployeeProfile extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		HttpSession session = request.getSession(false);
		Employee emp = (Employee) session.getAttribute("emp");
		
		
		
		if (emp != null) {
			RequestDispatcher rd = request.getRequestDispatcher("EmployeeProfile.jsp");
			rd.include(request, response);
		} else {
			out.print("<center>");	
			out.print("<br/><h3 style='color:red;'>Employee Record Not Found!!!</h3>");
			out.print("</center>");	
		}
		
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
